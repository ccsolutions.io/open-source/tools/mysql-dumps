### Description
This tool helps to make backups from mariadb/mysql instances, encrypting and uploading them to s3 buckets.
### Stack used
* [gnupg](https://www.gnupg.org/)
* [mysql cli](https://mysql.com/)
* [aws cli](https://aws.amazon.com/cli/)
### Environment variables
Listed in .env-example
### Restore from backup
In order to restore from backup first download from s3 bucket, decrypt the backup and upload it with the following commands:
```
$ gpg --decrypt backup.gz.gpg --output backup.gz 
$ gzip -d -f backup.gz
$ mysql -h$MARIADB_HOST -u$MARIADB_USER -p$MARIADB_ROOT_PASSWORD database_name < backup.sql
```
