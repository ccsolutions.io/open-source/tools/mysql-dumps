#!/bin/sh
source .env
if [[ -z "$MARIADB_USER" ]]; then
        echo "Must provide MARIADB_USER in environment."
        exit 2
fi
if [[ -z "$MARIADB_PASSWORD" ]]; then
        echo "Must provide MARIADB_PASSWORD in environment."
        exit 2
fi
if [[ -z "$MARIADB_ROOT_PASSWORD" ]]; then
        echo "Must provide MARIADB_ROOT_PASSWORD in environment."
        exit 2
fi
if [[ -z "$MARIADB_HOST" ]]; then
        echo "Must provide MARIADB_HOST in environment."
        exit 2
fi
if [[ -z "$MARIADB_PORT" ]]; then
        echo "No port provided, using default MARIADB_PORT 3306 instead"
        MARIADB_PORT="3306"
        exit 2
fi
if [[ -z "$GPG_KEY" ]]; then
        echo "Must provide GPG_KEY in environment."
        exit 2
fi
if [[ -z "$GPG_RECIPIENT" ]]; then
        echo "Must provide GPG_RECIPIENT in environment."
        exit 2
fi
if [[ -z "$S3_PATH" ]]; then
        echo "Must provide S3_PATH in environment."
        exit 2
fi
if [[ -z "$AWS_ACCESS_KEY_ID" ]]; then
        echo "Must provide AWS_ACCESS_KEY_ID in environment."
        exit 2
fi
if [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
        echo "Must provide AWS_SECRET_ACCESS_KEY in environment."
        exit 2
fi
if [[ -z "$S3_ENDPOINT" ]]; then
        echo "Must provide S3_ENDPOINT in environment."
        exit 2
fi
if [[ -z "$HEALTHCHECK_ID" ]]; then
        echo "Must provide HEALTHCHECK_ID (healthchecks.io) in environment."
        exit 2
fi
echo "GPG_KEY import is starting"
echo "$GPG_KEY" | gpg --batch --import --verbose
echo "GPG_KEY import is done"
if ! [ $? = "0" ]; then
  echo "GPG_KEY import failed"
  curl --retry 5 https://hc-ping.com/$HEALTHCHECK_ID/fail
else
echo "GPG_KEY import successful"
curl --retry 5 https://hc-ping.com/$HEALTHCHECK_ID
dbs=$(mysql -h$MARIADB_HOST -u$MARIADB_USER -p$MARIADB_ROOT_PASSWORD -N -B -e "show databases;")
echo Found Databases: $dbs
for db in $dbs; do
        if ! [ "$db" = "mysql" ] && ! [ "$db" = "sys" ] && ! [ "$db" = "information_schema" ] && ! [ "$db" = "performance_schema" ] && ! [ "$db" = "test" ]; then
                backup_name="$(date +%s)_$db.tar.gz"
                echo $backup_name
                mysqldump --single-transaction -h$MARIADB_HOST -u$MARIADB_USER -p$MARIADB_ROOT_PASSWORD --databases $db | gzip > $backup_name
                encrypted_name="$backup_name.gpg"
                gpg --always-trust -r $GPG_RECIPIENT -o $encrypted_name -e "$backup_name"
                if ! [ $? = "0" ]; then
                  echo "GPG encryption failed. Upload will not occur."
                  curl --retry 5 https://hc-ping.com/$HEALTHCHECK_ID/fail
                fi
                aws s3 cp $encrypted_name $S3_PATH --endpoint-url "https://$S3_ENDPOINT"
        fi
done
fi
rm *.gz*