FROM alpine:3.15.4

LABEL maintainer="Marco Wittmer <mwi@ccsolutions.io>"

RUN apk update

RUN apk upgrade

RUN apk add --no-cache \
        gnupg \
        wget \
        unzip \
        libseccomp \
        ca-certificates \
        python3-dev \
        py3-pip \
        libffi-dev \
        openssl-dev \
        gcc \
        libc-dev \
        make \
        bash \
        git \
        mysql-client \
        git  \
        aws-cli \
        curl

# cleanup
RUN rm -rf /var/cache/apk/*
COPY . .
CMD ["sh"]